from django.shortcuts import render
from .models import Card


# Create your views here.
def home(request, *args, **kwargs):
    card = Card.objects.all()
    create_card = {'card': card}
    return render(request, 'index.html', create_card)
